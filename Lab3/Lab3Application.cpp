#include <Lab3Application.h>
#include <Shaders.h>
#include <RenderCommands.h>

Lab3Application::Lab3Application(std::string appName, std::string appVersion) : GLFWApplication::GLFWApplication(appName, appVersion)
{
}

Lab3Application::~Lab3Application()
{
}

unsigned int Lab3Application::Run()
{
    /// Vertex Buffer Objects
    auto grid = GeometricTools::UnitGridGeometry2D<8, 8>();
    auto cube = GeometricTools::UnitCube3D;
    vboChessboard = std::make_shared<VertexBuffer>(grid.data(), sizeof(grid[0]) * grid.size());
    vboCube = std::make_shared<VertexBuffer>(&cube, sizeof(cube));

    // Vertex Buffer Layout of the vertex data of the chessboard and cube
    BufferLayout layout = BufferLayout({
    {ShaderDataType::Float3, "Position", 0},
    {ShaderDataType::Float4, "Colour", 1}
        });
    vboChessboard->SetLayout(layout);
    vboCube->SetLayout(layout);

    /// Element Buffer Objects
    auto indices = GeometricTools::UnitGridTopologyTriangle<8, 8>();
    auto indicesCube = GeometricTools::UnitCube3DTopology;
    // Element buffer for the chessboard
    std::shared_ptr<IndexBuffer> ibo = std::make_shared<IndexBuffer>(indices.data(), indices.size());
    // Element buffer for the cube
    std::shared_ptr<IndexBuffer> iboCube = std::make_shared<IndexBuffer>(indicesCube.data(), indicesCube.size());

    /// Vertex Array Object
    std::shared_ptr<VertexArray> vao = std::make_shared<VertexArray>();
    vao->Bind();
    vao->AddVertexBuffer(vboChessboard);
    vao->SetIndexBuffer(ibo);

    // The shader program for rendering the chessboard
    std::shared_ptr<Program> chessProgram = std::make_shared<Program>();
    if (!chessProgram->AddShader(GL_VERTEX_SHADER, vs_source))
        return EXIT_FAILURE;
    if (!chessProgram->AddShader(GL_FRAGMENT_SHADER, fs_source))
        return EXIT_FAILURE;
    if (!chessProgram->Link())
        return EXIT_FAILURE;
    chessProgram->Bind();

    // The Shader program for rendering the cube
    std::shared_ptr<Program> cubeProgram = std::make_shared<Program>();
    if (!cubeProgram->AddShader(GL_VERTEX_SHADER, cube_vs_source))
        return EXIT_FAILURE;
    if (!cubeProgram->AddShader(GL_FRAGMENT_SHADER, cube_fs_source))
        return EXIT_FAILURE;
    if (!cubeProgram->Link())
        return EXIT_FAILURE;
    //cubeProgram->Bind();

    fprintf(stdout, "Setup Done\n");

    // View matrix
    glm::mat4 view = glm::lookAt(glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(0.0, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    projection = glm::perspective(glm::radians(60.f), (float)(wWidth / wHeight), 0.1f, 100.0f);

    // The chessboard matrices
    auto chessboardRotation = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    auto chessboardTranslation = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
    auto chessboardScale = glm::scale(glm::mat4(1.0f), glm::vec3(8.0f, 8.0f, 8.0f));
    glm::mat4 chessModel = chessboardTranslation * chessboardRotation * chessboardScale;
    glm::mat4 chessMvp = projection * view * chessModel;
    glUniformMatrix4fv(2, 1, GL_FALSE, &chessMvp[0][0]);

    // Cube model matrix
    glm::mat4 cubeModel = glm::mat4(1.0);

    // Current time
    float time = 0.f;
    // Delta Time
    float delta = 0.f;
    // Time of the last frame
    float lastFrame = 0.f;

    // Background colour
    glm::vec4 bgCol = glm::vec4( 0.4f, 0.4f, 0.4f, 1.0f);
    // The colour to paint the sides of the cube
    glm::vec4 white = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    // The colour to paint the lines when rendering in polygon mode set to GL_LINE
    glm::vec4 black = glm::vec4(.0f, .0f, .0f, 1.0f);

    glEnable(GL_DEPTH_TEST);

    /// Rendering loop
    do {
        RenderCommands::ClearColor(bgCol);
        RenderCommands::ClearDepth(1.0f);

        time = (float)glfwGetTime();
        delta = time - lastFrame;
        lastFrame = time;

        chessProgram->Bind();

        vao->AddVertexBuffer(vboChessboard);
        vao->SetIndexBuffer(ibo);
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        {
            cubeModel = glm::rotate(cubeModel, glm::radians(delta * -45.0f), glm::vec3(1, 0, 0));
        }

        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        {
            cubeModel = glm::rotate(cubeModel, glm::radians(delta * 45.0f), glm::vec3(1, 0, 0));
        }

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        {
            cubeModel = glm::rotate(cubeModel, glm::radians(delta * 45.0f), glm::vec3(0, 1, 0));
        }

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        {
            cubeModel = glm::rotate(cubeModel, glm::radians(delta * -45.0f), glm::vec3(0, 1, 0));
        }

        glm::mat4 chessMvp = projection * view * chessModel;
        chessProgram->UploadUniformMat4("mvp", chessMvp);

        RenderCommands::SetSolidMode(GL_FRONT_AND_BACK);
        RenderCommands::DrawIndex(vao, GL_TRIANGLES);

        cubeProgram->Bind();
        vao->AddVertexBuffer(vboCube);
        vao->SetIndexBuffer(iboCube);
        glm::mat4 cubeMvp = projection * view * cubeModel;
        cubeProgram->UploadUniformMat4("mvp", cubeMvp);
        cubeProgram->UploadUniformFloat4("u_colour", white);
        RenderCommands::DrawIndex(vao, GL_TRIANGLES);
        cubeProgram->UploadUniformFloat4("u_colour", black);
        RenderCommands::SetWireframeMode(GL_FRONT_AND_BACK);
        RenderCommands::DrawIndex(vao, GL_TRIANGLES);

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while (!glfwWindowShouldClose(window));

    return EXIT_SUCCESS;
}

void Lab3Application::onKey(int key, int action)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }

    if (key == GLFW_KEY_UP && action == GLFW_PRESS && (currentTileY < 7))
    {
        float colour = (currentTileX + currentTileY++) % 2;
        glm::vec4 tileCol = glm::vec4(colour);
        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (currentTile += 8) * tileSize;

        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }
    }

    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && (currentTileY > 0))
    {
        float colour = (currentTileX + currentTileY--) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (currentTile -= 8) * tileSize;
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }

    }

    if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && (currentTileX % 8 < 7))
    {
        float colour = (currentTileY + currentTileX++) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (++currentTile) * tileSize;
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }
    }

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && (currentTileX % 8 > 0))
    {
        float colour = (currentTileY + currentTileX--) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (--currentTile) * tileSize;
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }
    }
}

void Lab3Application::onResize(int width, int height)
{
    GLFWApplication::onResize(width, height);
    projection = glm::perspective(glm::radians(60.f), (float)width / (float)height, 0.1f, 100.0f);
    glViewport(0, 0, width, height);
}