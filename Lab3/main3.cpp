#include "Lab3Application.h"

int main(int argc, char* argv[])
{
    Lab3Application app("lab3", "1.0");
    app.ParseArguments(argc, argv);
    app.Init();
    return app.Run();
}