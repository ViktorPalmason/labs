#include <glad/gl.h>
// Vertex Shader source
const GLchar* vs_source = R"(
    #version 430 core

    layout(location = 0) in vec3 pos;
    layout(location = 1) in vec4 col;

    layout(location = 2) uniform mat4 mvp;
    out vec4 vs_colour;

    void main()
    {
        gl_Position = mvp * vec4(pos, 1.0);
        vs_colour = col;
    }
)";
// Fragment Shader Source
const GLchar* fs_source = R"(
    #version 430 core
    
    in vec4 vs_colour;
    out vec4 color;
    
    void main()
    {
        color = vs_colour;
    }
)";

// Vertex Shader Source for the 3D Cube
const GLchar* cube_vs_source = R"(
    #version 430 core

    layout(location = 0) in vec4 pos;
    layout(location = 1) in vec4 col;

    layout(location = 2) uniform mat4 mvp;
    
    void main()
    {
        gl_Position = mvp * pos;
    }
)";

const GLchar* cube_fs_source = R"(
    #version 430 core

    layout(location = 3) uniform vec4 u_colour;
    out vec4 colour;
    void main()
    {
        colour = u_colour;
    }        
)";