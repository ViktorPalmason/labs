#ifndef LAB3APPLICATION_H_
#define LAB3APPLICATION_H_

#include <GLFWApplication.h>
#include <GeometricTools.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>
#include <ShaderDataTypes.h>
#include <VertexArray.h>
#include <Program.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

class Lab3Application : public GLFWApplication
{
public:
    // Projection matrix
    glm::mat4 projection = glm::mat4(1);
    // The Y coordinate of the current selecte tile of the chessboard.
    int currentTileY = 0;
    // The X coordinate of the current selecte tile of the chessboard.
    int currentTileX = 0;
    // The current selected tile of the chessboard, this is numbered from 0 to 63.
    int currentTile = 0;
    // The size of each chessboard tile in bytes.
    // This is equivalent to the size in bytes of the 2D square geometry. 
    float tileSize = sizeof(float) * 28.0f;
    // The size of each vertices in bytes.
    float vertSize = sizeof(float) * 7.0f;
    // The offset to the colour values in each vertices. 
    float colOffset = sizeof(float) * 3.0f;
    // The colour that highliths a selected tile.
    float selCol[4] = { 0.f, 1.0f, 0.0f, 1.0f };

    // The Vertex Buffer used for storing the data of the chessboard
    std::shared_ptr<VertexBuffer> vboChessboard;
    // The Vertex Buffer used for storing the data of the cube
    std::shared_ptr<VertexBuffer> vboCube;

    Lab3Application(std::string name, std::string version);
    ~Lab3Application();

    /// Inherited via GLFWApplication

    unsigned int Run();

    void onKey(int key, int action);
    void onResize(int width, int action);
};

#endif 