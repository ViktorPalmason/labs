#include "VertexArray.h"

VertexArray::VertexArray()
{
    //BindingIdx = 0;
    glCreateVertexArrays(1, &ID);
}

VertexArray::~VertexArray()
{
    this->Unbind();
    glDeleteVertexArrays(1, &ID);
}

void VertexArray::Bind()
{
    glBindVertexArray(ID);
}

void VertexArray::Unbind()
{
    glBindVertexArray(0);
}

void VertexArray::AddVertexBuffer(const std::shared_ptr<VertexBuffer>& vertexBuffer)
{
    // Define the layout of the attributes in the VAO
    for (auto&& attrib : vertexBuffer->Layout.GetAttributes())
    {
        glVertexArrayAttribBinding(ID, attrib.Location, vertexBuffer->VertexBufferID);
        glVertexArrayAttribFormat(ID,
            attrib.Location,
            ShaderDataTypeComponentCount(attrib.Type),
            ShaderDataTypeToOpenGLBaseType(attrib.Type),
            attrib.Normalized,
            attrib.Offset);
        glEnableVertexArrayAttrib(ID, attrib.Location);
    }
    // Bind the VBO to the VAO
    glVertexArrayVertexBuffer(ID, vertexBuffer->VertexBufferID, vertexBuffer->VertexBufferID, 0, vertexBuffer->Layout.GetStride());
    // Add the VBO to the list of VBO's associated with the VAO
    VertexBuffers.push_back(vertexBuffer);
    //BindingIdx++;
}

void VertexArray::SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer)
{
    glVertexArrayElementBuffer(ID, indexBuffer->IndexBufferID);
    IdxBuffer = indexBuffer;
}