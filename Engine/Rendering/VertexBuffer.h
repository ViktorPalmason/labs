#ifndef VERTEXBUFFER_H_
#define VERTEXBUFFER_H_

#include <glad/gl.h>
#include <BufferLayout.h>

class VertexBuffer
{
public:
    GLuint VertexBufferID;
    BufferLayout Layout;

    VertexBuffer(const void* vertices, GLsizei size);
    ~VertexBuffer();

    void Bind() const;
    void Unbind() const;
    void BufferSubData(GLintptr offset, GLsizeiptr size, const void* data) const;

    const BufferLayout& GetLayout() { return Layout; }
    void SetLayout(const BufferLayout& layout) { Layout = layout; }
};

#endif
