#ifndef PROGRAM_H_
#define PROGRAM_H_

#include <glad/gl.h>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <string>
#include <map>

class Program
{
private:
    GLuint ID;
    std::map<std::string, GLuint> Uniforms;
public:
    Program();
    ~Program();

    void Bind() const;
    void Unbind() const;
    void UploadUniformFloat1(const std::string& name, const float& value);
    void UploadUniformFloat2(const std::string& name, const glm::vec2& vector);
    void UploadUniformFloat4(const std::string& name, const glm::vec4& vector);
    void UploadUniformMat4(const std::string& name, const glm::mat4& vector);

    bool AddShader(GLenum type, const GLchar* &source);
    bool Link();
};

#endif