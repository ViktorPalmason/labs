#include <Program.h>

Program::Program()
{
    ID = glCreateProgram();
    Uniforms = std::map<std::string, GLuint>();
}

Program::~Program()
{
    glDeleteProgram(ID);
}

void Program::Bind() const
{
    glUseProgram(ID);
}

void Program::Unbind() const
{
    glUseProgram(0);
}

void Program::UploadUniformFloat1(const std::string& name, const float& value)
{
    GLint loc;
    if (Uniforms.count(name) == 0)
    {
        loc = glGetUniformLocation(ID, name.c_str());
        Uniforms[name] = loc;
    }
    else
        loc = Uniforms[name];

    glUniform1fv(loc, 1, &value);
}

void Program::UploadUniformFloat2(const std::string& name, const glm::vec2& value)
{
    GLint loc;
    if (Uniforms.count(name) == 0)
    {
        loc = glGetUniformLocation(ID, name.c_str());
        Uniforms[name] = loc;
    }
    else
        loc = Uniforms[name];

    glUniform2fv(loc, 1, &value[0]);
}

void Program::UploadUniformFloat4(const std::string& name, const glm::vec4& value)
{
    GLint loc;
    if (Uniforms.count(name) == 0) {
        loc = glGetUniformLocation(ID, name.c_str());
        Uniforms[name] = loc;
    }
    else
        loc = Uniforms[name];

    glUniform4fv(loc, 1, &value[0]);
}

void Program::UploadUniformMat4(const std::string& name, const glm::mat4& value)
{
    GLint loc;
    if (Uniforms.count(name) == 0)
    {
        loc = glGetUniformLocation(ID, name.c_str());
        Uniforms[name] = loc;
    }
    else
        loc = Uniforms[name];

    glUniformMatrix4fv(loc, 1, GL_FALSE, &value[0][0]);
}

bool Program::AddShader(GLenum type, const GLchar*& source)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, 0);
    glCompileShader(shader);

    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (!status)
    {
        GLchar* info = "";
        GLint size;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &size);
        glGetShaderInfoLog(shader, size, 0, info);
        fprintf(stderr, "ERROR! Failed to compile %s.\nLog: %s\n", (type == GL_VERTEX_SHADER ? "VERTEX SHADER" : "FRAGMENT SHADER"), info);
        glDeleteShader(shader);
        return false;
    }

    glAttachShader(ID, shader);
    glDeleteShader(shader);
    return true;
}

bool Program::Link()
{
    glLinkProgram(ID);
    GLint status;
    glGetProgramiv(ID, GL_LINK_STATUS, &status);
    if (!status)
    {
        GLchar* info = "";
        GLint logSize;
        glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &logSize);
        glGetProgramInfoLog(ID, logSize, 0, info);
        fprintf(stderr, "ERROR! Failed to link the rendering program.\nLOG: %s\n", info);
        return false;
    }
    return true;
}