#include <array>
#include <vector>

namespace GeometricTools
{
    constexpr std::array<float, 3*6> UnitTriangle2D = 
    {
        -0.5, -0.5f, 1.0f, 0.f, 0.f, 1.0f,
        0.5f, -0.5f, 1.0f, 0.f, 0.f, 1.0f,
        0.0f, 0.5f, 1.0f, 0.f, 0.f, 1.0f
    };

    constexpr std::array<float, 6*6> UnitSquare2D =
    {
        -0.5f, -0.5f, 0.f, 1.0f, 0.f, 1.0f,
        0.5f, -0.5f, 0.f, 1.0f, 0.f, 1.0f,
        -0.5, 0.5f, 0.f, 1.0f, 0.f, 1.0f,

        0.5f, -0.5f, 0.f, 1.0f, 0.f, 1.0f,
        0.5f, 0.5f, 0.f, 1.0f, 0.f, 1.0f,
        -0.5f, 0.5f, 0.f, 1.0f, 0.f, 1.0f,
    };

    constexpr std::array<float, 8 * 7> UnitCube3D =
    {
        // Front Square
       -0.5f, -0.5f, 0.5f, 1.f, 1.0f, 1.f, 1.0f,   // 0
        0.5f, -0.5f, 0.5f, 1.f, 1.0f, 1.f, 1.0f,   // 1
       -0.5f,  0.5f, 0.5f, 1.f, 1.0f, 1.f, 1.0f,   // 2
        0.5f,  0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f, // 3
        // Back Square                             
       -0.5f, -0.5f, -0.5f, 1.f, 1.0f, 1.f, 1.0f,  // 4
        0.5f, -0.5f, -0.5f, 1.f, 1.0f, 1.f, 1.0f,  // 5
       -0.5f,  0.5f, -0.5f, 1.f, 1.0f, 1.f, 1.0f,  // 6
        0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f,// 7
    };

    constexpr std::array<unsigned int, 3 * 12> UnitCube3DTopology =
    {
        0, 1, 2,
        1, 3, 2,

        1, 5, 3,
        5, 7, 3,

        5, 4, 7,
        4, 6, 7,

        4, 0, 6,
        0, 2, 6,

        2, 3, 6,
        3, 7, 6,

        4, 5, 0,
        5, 1, 0
    };

    template <int  x=1, int y=1>
    constexpr std::vector<float> UnitGridGeometry2D()
    {
        std::array<float, 7 * 4> grid;
        std::vector<float> gridVec;
        float posXCoord = 1.f / x;
        float posYCoord = 1.f / y;

        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                float colour = (float)((i + j) % 2);
                float xCoord = (posXCoord * j);
                float yCoord = (posYCoord * i);
                grid =
                {
                   -0.5f + xCoord,                 -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f,
                   (-0.5f + posXCoord) + xCoord,   -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f,
                   -0.5f + xCoord,                 (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f,
                   (-0.5f + posXCoord) + xCoord,   (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f,
                };
                gridVec.insert(gridVec.end(), begin(grid), end(grid));
            }
            std::cout << std::endl;
        }
        return gridVec;
    };

    template <int x=1, int y=1>
    constexpr std::vector<unsigned int> UnitGridTopologyTriangle()
    {
        std::vector<unsigned int> gridInd;
        for (int i = 0; i < y * x; i++)
        {
            gridInd.push_back(0 + (i * 4));
            gridInd.push_back(1 + (i * 4));
            gridInd.push_back(2 + (i * 4));

            gridInd.push_back(1 + (i * 4));
            gridInd.push_back(3 + (i * 4));
            gridInd.push_back(2 + (i * 4));
        }

        return gridInd;
    };

    template <int  x = 1, int y = 1>
    constexpr std::vector<float> UnitGridGeometry2DWTcoords()
    {
        std::array<float, 9 * 4> grid;
        std::vector<float> gridVec;
        float posXCoord = 1.f / x;
        float posYCoord = 1.f / y;

        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                float colour = (float)((i + j) % 2);
                float xCoord = (posXCoord * j);
                float yCoord = (posYCoord * i);
                grid =
                {
                   -0.5f + xCoord,                 -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f, 0.0f, 0.0f,
                   (-0.5f + posXCoord) + xCoord,   -0.5f + yCoord,              0.0f, colour, colour, colour, 1.0f, 0.0f, 1.0f,
                   -0.5f + xCoord,                 (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f, 1.0f, 0.0f,
                   (-0.5f + posXCoord) + xCoord,   (-0.5f + posYCoord) + yCoord, 0.0f, colour, colour, colour, 1.0f, 1.0f, 1.0f
                };
                gridVec.insert(gridVec.end(), begin(grid), end(grid));
            }
            std::cout << std::endl;
        }
        return gridVec;
    };
}