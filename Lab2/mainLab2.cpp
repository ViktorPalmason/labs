#include "iostream"
#include "Lab2Application.h"

int main(int argc, char* argv[])
{
    Lab2Application app("lab2", "1.0");
    app.ParseArguments(argc, argv);
    app.Init();
    return app.Run();
}