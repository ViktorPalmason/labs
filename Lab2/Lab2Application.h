#include <GLFWApplication.h>
#include <GeometricTools.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>
#include <ShaderDataTypes.h>
#include <VertexArray.h>
#include <Program.h>

#include <vector>

class Lab2Application : public GLFWApplication
{
public:
    Lab2Application(std::string name, std::string version);
    ~Lab2Application();

    /// Inherited via GLFWApplication
    unsigned int Run();
};