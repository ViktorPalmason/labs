#include <glad/gl.h>
// Vertex Shader source
const GLchar* vs_source = R"(
    #version 430 core

    layout(location = 0) in vec2 pos;
    layout(location = 1) in vec4 col;

    layout(location = 2) uniform vec2 selector;
    out vec4 vs_colour;

    void main()
    {
        gl_Position = vec4(pos, 0.0, 1.0);
        if(selector == pos)
            vs_colour = vec4(1.0, 1.0, 0, 1.0f);
        else
            vs_colour = col;
    }
)";
// Fragment Shader Source
const GLchar* fs_source = R"(
    #version 430 core
    
    in vec4 vs_colour;
    out vec4 color;
    
    void main()
    {
        color = vs_colour;
    }
)";