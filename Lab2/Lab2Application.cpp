#include "Lab2Application.h"
#include "Shaders.h"

Lab2Application::Lab2Application(std::string appName, std::string appVersion) : GLFWApplication::GLFWApplication(appName, appVersion)
{

}

Lab2Application::~Lab2Application()
{

}

unsigned int Lab2Application::Run()
{
    /// Vertex Buffer Object
    auto grid = GeometricTools::UnitGridGeometry2D<8, 8>();
    std::shared_ptr<VertexBuffer> vboChessboard = std::make_shared<VertexBuffer>(grid.data(), sizeof(grid[0]) * grid.size());
    BufferLayout layout = BufferLayout({
        {ShaderDataType::Float3, "Position", 0},
        {ShaderDataType::Float4, "Colour", 1}
        });
    vboChessboard->SetLayout(layout);

    /// Element Buffer Object
    std::vector<unsigned int> indices = GeometricTools::UnitGridTopologyTriangle<8, 8>();
    std::shared_ptr<IndexBuffer> ibo = std::make_shared<IndexBuffer>(indices.data(), indices.size());

    /// Vertex Array Object
    std::shared_ptr<VertexArray> vao = std::make_shared<VertexArray>();
    vao->Bind();
    vao->AddVertexBuffer(vboChessboard);
    vao->SetIndexBuffer(ibo);

    std::shared_ptr<Program> program = std::make_shared<Program>();
    if (!program->AddShader(GL_VERTEX_SHADER, vs_source))
        return EXIT_FAILURE;
    if (!program->AddShader(GL_FRAGMENT_SHADER, fs_source))
        return EXIT_FAILURE;
    if (!program->Link())
        return EXIT_FAILURE;
    program->Bind();

    fprintf(stdout, "Setup Done\n");

    /// Rendering loop
    float bgCol[] = { 0.4f, 0.4f, 0.4f, 1.0f };
    float selCol[] = { 0.f, 1.0f, 0.0f, 1.0f };

    bool upPress = false;
    bool downPress = false;
    bool rightPress = false;
    bool leftPress = false;
    int currentX = 0;
    int currentY = 0;
    int currentTile = 0;
    glm::vec2 position = { 0.0f, 0.0f };
    float tileSize = sizeof(float) * 24.0f;
    float vertSize = sizeof(float) * 6.0f;
    float colOffset = sizeof(float) * 2.0f;
    do {
        glClearBufferfv(GL_COLOR, 0, bgCol);

        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && !upPress)
        {
            upPress = true;
            if (currentY < 7)
            {
                float colour = (float)((currentX + currentY++) % 2);
                float revertCol[] = { colour, colour, colour, 1.0f };

                float prevTileOffset = currentTile * tileSize;
                float nextTileOffset = (currentTile += 8) * tileSize;

                position = position + glm::vec2({ 0.0f,(1.0f / 8.0f) });
                program->UploadUniformFloat2("selector", position);

                for (int i = 0; i < 4; i++)
                {
                    float vertOffset = i * vertSize;
                    vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(revertCol), revertCol);
                    vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
                }
            }
        }
        else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE)
        {
            upPress = false;
        }

        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && !downPress)
        {
            downPress = true;

            if (currentY > 0)
            {
                float colour = (float)((currentX + currentY--) % 2);
                float revertCol[] = { colour, colour, colour, 1.0f };

                float prevTileOffset = currentTile * tileSize;
                float nextTileOffset = (currentTile -= 8) * tileSize;
                for (int i = 0; i < 4; i++)
                {
                    float vertOffset = i * vertSize;
                    vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(revertCol), revertCol);
                    vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
                }
            }
        }
        else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE)
        {
            downPress = false;
        }

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && !rightPress)
        {
            rightPress = true;

            if (currentX % 8 < 7)
            {
                float colour = (float)((currentY + currentX++) % 2);
                float revertCol[] = { colour, colour, colour, 1.0f };

                float prevTileOffset = currentTile * tileSize;
                float nextTileOffset = (++currentTile) * tileSize;
                for (int i = 0; i < 4; i++)
                {
                    float vertOffset = i * vertSize;
                    vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(revertCol), revertCol);
                    vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
                }
            }
        }
        else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE)
        {
            rightPress = false;
        }

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && !leftPress)
        {
            leftPress = true;

            if (currentX % 8 > 0)
            {
                float colour = (float)((currentY + currentX--) % 2);
                float revertCol[] = { colour, colour, colour, 1.0f };

                float prevTileOffset = currentTile * tileSize;
                float nextTileOffset = (--currentTile) * tileSize;
                for (int i = 0; i < 4; i++)
                {
                    float vertOffset = i * vertSize;
                    vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(revertCol), revertCol);
                    vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
                }
            }
        }
        else if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE)
        {
            leftPress = false;
        }

        glDrawElements(GL_TRIANGLES, ibo->GetCount(), GL_UNSIGNED_INT, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while (!glfwWindowShouldClose(window));

    return EXIT_SUCCESS;
}