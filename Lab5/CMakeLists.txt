cmake_minimum_required(VERSION 3.15...3.24)

project(Lab5)

add_executable(Lab5 main5.cpp 
    Lab5Application.cpp Lab5Application.h
    Shaders.h)
target_include_directories(Lab5 PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(Lab5 PUBLIC Engine::GLFWApplication Engine::GeometricTools Engine::Rendering)

add_custom_command(
    TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_CURRENT_SOURCE_DIR}/resources/textures/Old_Brown_Paper.jpg
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/resources/textures/Old_Brown_Paper.jpg
)

add_custom_command(
    TARGET ${PROJECT_NAME} POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_CURRENT_SOURCE_DIR}/resources/textures/Brown_Tile.jpg
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/resources/textures/Brown_Tile.jpg
)

target_compile_definitions(${PROJECT_NAME} PRIVATE
    TEXTURES_DIR="${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/resources/textures")