#include <Lab5Application.h>

int main(int argc, char* argv[])
{
    Lab5Application app("lab5", "1.0");
    app.ParseArguments(argc, argv);
    app.Init();
    return app.Run();
}