#include <Lab5Application.h>
#include <Shaders.h>
#include <RenderCommands.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

Lab5Application::Lab5Application(std::string appName, std::string appVersion) : GLFWApplication::GLFWApplication(appName, appVersion)
{
}

Lab5Application::~Lab5Application()
{
}

unsigned int Lab5Application::Run()
{
    /// Vertex Buffer Objects
    auto grid = GeometricTools::UnitGridGeometry2D<8, 8>();
    auto cube = GeometricTools::UnitCube3D;
    auto texGrid = GeometricTools::UnitGridGeometry2DWTcoords<8, 8>();
    vboChessboard = std::make_shared<VertexBuffer>(grid.data(), sizeof(grid[0]) * grid.size());
    auto vboChessboardTex = std::make_shared<VertexBuffer>(texGrid.data(), sizeof(texGrid[0]) * texGrid.size());
    vboChessboard = std::make_shared<VertexBuffer>(texGrid.data(), sizeof(texGrid[0]) * texGrid.size());
    vboCube = std::make_shared<VertexBuffer>(&cube, sizeof(cube));
    TextureManager* texManager = TextureManager::GetInstance();

    // Vertex Buffer Layout of the vertex data of the chessboard and cube
    BufferLayout chessTexLayout = BufferLayout{
        {ShaderDataType::Float3, "Position", 0},
        {ShaderDataType::Float4, "Colour", 1},
        {ShaderDataType::Float2, "TexCoords", 2}
    };
    vboChessboardTex->SetLayout(chessTexLayout);

    BufferLayout cubeLayout = BufferLayout({
    {ShaderDataType::Float3, "Position", 0},
    {ShaderDataType::Float4, "Colour", 1}
        });
    vboCube->SetLayout(cubeLayout);
    vboChessboard->SetLayout(chessTexLayout);

    /// Element Buffer Objects
    auto indices = GeometricTools::UnitGridTopologyTriangle<8, 8>();
    auto indicesCube = GeometricTools::UnitCube3DTopology;
    // Element buffer for the chessboard
    std::shared_ptr<IndexBuffer> ibo = std::make_shared<IndexBuffer>(indices.data(), indices.size());
    // Element buffer for the cube
    std::shared_ptr<IndexBuffer> iboCube = std::make_shared<IndexBuffer>(indicesCube.data(), indicesCube.size());

    bool success = texManager->LoadTexture2DRGBA("chessboardTex", std::string(TEXTURES_DIR) + "/Old_Brown_Paper.jpg", 0);
    if (!success)
    {
        return EXIT_FAILURE;
    }
    success = texManager->LoadCubeMapRGBA("cubeTex", std::string(TEXTURES_DIR) + "/Brown_Tile.jpg", 1);
    if (!success)
        return EXIT_FAILURE;

    /// Vertex Array Object
    std::shared_ptr<VertexArray> vao = std::make_shared<VertexArray>();
    vao->Bind();
    vao->AddVertexBuffer(vboChessboard);
    vao->SetIndexBuffer(ibo);

    // The shader program for rendering the chessboard
    std::shared_ptr<Program> chessProgram = std::make_shared<Program>();
    if (!chessProgram->AddShader(GL_VERTEX_SHADER, chess_vs_source))
        return EXIT_FAILURE;
    if (!chessProgram->AddShader(GL_FRAGMENT_SHADER, chess_fs_source))
        return EXIT_FAILURE;
    if (!chessProgram->Link())
        return EXIT_FAILURE;
    chessProgram->Bind();

    // The Shader program for rendering the cube
    std::shared_ptr<Program> cubeProgram = std::make_shared<Program>();
    if (!cubeProgram->AddShader(GL_VERTEX_SHADER, cube_vs_source))
        return EXIT_FAILURE;
    if (!cubeProgram->AddShader(GL_FRAGMENT_SHADER, cube_fs_source))
        return EXIT_FAILURE;
    if (!cubeProgram->Link())
        return EXIT_FAILURE;
    //cubeProgram->Bind();

    fprintf(stdout, "Setup Done\n");

    // The chessboard matrices
    auto chessboardRotation = glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    auto chessboardTranslation = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
    auto chessboardScale = glm::scale(glm::mat4(1.0f), glm::vec3(8.0f, 8.0f, 8.0f));
    glm::mat4 chessModel = chessboardTranslation * chessboardRotation * chessboardScale;

    orthoCam.SetFrustrum({ -8.0f, 8.0f, -8.0f, 8.0f, 10.0f, -100.0f });
    orthoCam.SetPosition(glm::vec3(0, 0, 1));
    persCam.SetLookAt(glm::vec3(0, 0, 0));
    persCam.SetPosition(glm::vec3(0, 5, 10));
    persCam.SetFrustrum({ 45.0f, (float)wWidth, (float)wHeight, 1.0f, -1.0f });
    glm::mat4 chessMvp = persCam.GetViewProjectionMatrix() * chessModel;
    chessProgram->UploadUniformMat4("mvp", chessMvp);

    // Cube model matrix
    glm::mat4 cubeModel = glm::mat4(1.0);
    glm::mat4 cubeRotX = glm::mat4(1.0);
    glm::mat4 cubeRotY = glm::mat4(1.0);
    glm::mat4 iden = glm::mat4(1);

    // Current time
    float time = 0.f;
    // Delta Time
    float delta = 0.f;
    // Time of the last frame
    float lastFrame = 0.f;

    // Background colour
    glm::vec4 bgCol = glm::vec4( 0.4f, 0.4f, 0.4f, 1.0f);
    float ambientStrength = 1.0f;
    bool up = false;
    bool down = true;

    // Depth Testing
    glEnable(GL_DEPTH_TEST);
    // Blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Culling
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);

    /// Rendering loop
    do {
        RenderCommands::ClearColor(ambientStrength *bgCol);
        RenderCommands::ClearDepth(1.f);

        time = (float)glfwGetTime();
        delta = time - lastFrame;
        lastFrame = time;

        if (ambientStrength <= 0)
        {
            down = false;
            up = true;
        }
        else if (ambientStrength >= 1) {
            down = true;
            up = false;
        }

        if (down)
            ambientStrength -= delta*0.3;

        if (up)
            ambientStrength += delta*0.3;

        chessProgram->Bind();

        //vao->AddVertexBuffer(vboChessboardTex);
        vao->AddVertexBuffer(vboChessboard);
        vao->SetIndexBuffer(ibo);
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        {
            cubeRotX = glm::rotate(iden, glm::radians(-2.0f), glm::vec3(1, 0, 0));
            cubeModel =  cubeRotX * cubeModel;
        }

        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        {
            cubeRotX = glm::rotate(iden, glm::radians(2.0f), glm::vec3(1, 0, 0));
            cubeModel = cubeRotX * cubeModel;
        }

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        {
            cubeRotY = glm::rotate(iden, glm::radians(2.0f), glm::vec3(0, 1, 0));
            cubeModel = cubeRotY * cubeModel;
        }

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        {
            cubeRotY = glm::rotate(iden, glm::radians(-2.0f), glm::vec3(0, 1, 0));
            cubeModel = cubeRotY * cubeModel;
        }
;
        chessMvp = persCam.GetViewProjectionMatrix() * chessModel;
        chessProgram->UploadUniformMat4("mvp", chessMvp);
        chessProgram->UploadUniformFloat1("u_ambientStrength", ambientStrength);
        RenderCommands::DrawIndex(vao, GL_TRIANGLES);

        cubeProgram->Bind();
        vao->AddVertexBuffer(vboCube);
        vao->SetIndexBuffer(iboCube);
        glm::mat4 cubeMvp = persCam.GetViewProjectionMatrix() * cubeModel;
        cubeProgram->UploadUniformMat4("mvp", cubeMvp);
        cubeProgram->UploadUniformFloat4("u_colour", uColourCube);
        cubeProgram->UploadUniformFloat1("u_ambientStrength", ambientStrength);
        RenderCommands::DrawIndex(vao, GL_TRIANGLES);

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while (!glfwWindowShouldClose(window));

    return EXIT_SUCCESS;
}

void Lab5Application::onKey(int key, int action)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }

    if (key == GLFW_KEY_UP && action == GLFW_PRESS && (currentTileY < 7))
    {
        float colour = (currentTileX + currentTileY++) % 2;
        glm::vec4 tileCol = glm::vec4(colour);
        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (currentTile += 8) * tileSize;

        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }
    }

    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS && (currentTileY > 0))
    {
        float colour = (currentTileX + currentTileY--) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (currentTile -= 8) * tileSize;
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }

    }

    if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS && (currentTileX % 8 < 7))
    {
        float colour = (currentTileY + currentTileX++) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (++currentTile) * tileSize;
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }
    }

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS && (currentTileX % 8 > 0))
    {
        float colour = (currentTileY + currentTileX--) % 2;
        glm::vec4 tileCol = glm::vec4(colour);

        float prevTileOffset = currentTile * tileSize;
        float nextTileOffset = (--currentTile) * tileSize;
        for (int i = 0; i < 4; i++)
        {
            float vertOffset = i * vertSize;
            vboChessboard->BufferSubData(prevTileOffset + vertOffset + colOffset, sizeof(tileCol), &tileCol);
            vboChessboard->BufferSubData(nextTileOffset + vertOffset + colOffset, sizeof(selCol), selCol);
        }
    }

    if (key == GLFW_KEY_0 && action == GLFW_PRESS)
    {
        uColourCube = glm::vec4(1);
    }

    if (key == GLFW_KEY_1 && action == GLFW_PRESS)
    {
        uColourCube = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
    }

    if (key == GLFW_KEY_2 && action == GLFW_PRESS)
    {
        uColourCube = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
    }

    if (key == GLFW_KEY_3 && action == GLFW_PRESS)
    {
        uColourCube = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
    }
}

void Lab5Application::onResize(int width, int height)
{
    GLFWApplication::onResize(width, height);
    persCam.SetFrustrum({ 45.0f, (float)width, (float)height, 1.0f, -1.0f });
    glViewport(0, 0, width, height);
}