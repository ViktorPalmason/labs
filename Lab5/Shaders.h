#include <glad/gl.h>
// Vertex Shader source
const GLchar* chess_vs_source = R"(
    #version 430 core

    layout(location = 0) in vec3 pos;
    layout(location = 1) in vec4 col;
    layout(location = 2) in vec2 texCoords;

    layout(location = 2) uniform mat4 mvp;
    out vec2 vs_texCoords;
    out vec4 vs_colour;

    void main()
    {
        gl_Position = mvp * vec4(pos, 1.0);
        vs_texCoords = texCoords;
        vs_colour = col;
    }
)";
// Fragment Shader Source
const GLchar* chess_fs_source = R"(
    #version 430 core
    
    in vec2 vs_texCoords;
    in vec4 vs_colour;
    layout(binding=0) uniform sampler2D u_floorTextureSampler;
    uniform float u_ambientStrength=1.0;

    out vec4 color;
    
    void main()
    {
        color = u_ambientStrength * mix(vs_colour, texture(u_floorTextureSampler, vs_texCoords), 0.7);
    }
)";

// Vertex Shader Source for the 3D Cube
const GLchar* cube_vs_source = R"(
    #version 430 core

    layout(location = 0) in vec4 pos;
    layout(location = 1) in vec4 col;

    layout(location = 2) uniform mat4 mvp;
    out vec3 vs_pos;
    
    void main()
    {
        gl_Position = mvp * pos;
        vs_pos = pos.xyz;
    }
)";

const GLchar* cube_fs_source = R"(
    #version 430 core

    layout(location = 3) uniform vec4 u_colour;
    uniform float u_ambientStrength = 1.0;
    layout(binding=1) uniform samplerCube uTexture;
    in vec3 vs_pos;
    out vec4 colour;

    void main()
    {
        colour = u_ambientStrength * (vec4(u_colour.rgb, 1.0) * texture(uTexture, vs_pos));
        //colour = u_ambientStrength * colour;
    }        
)";