#include <Lab4Application.h>

int main(int argc, char* argv[])
{
    Lab4Application app("lab4", "1.0");
    app.ParseArguments(argc, argv);
    app.Init();
    return app.Run();
}