#include <iostream>
#include <algorithm>
#include <string>
#include <math.h>

#include <tclap/CmdLine.h>
#include <glad/gl.h>
#include <GLFW/glfw3.h>


void GLFWErrorCallback(int code, const char* description)
{
    fprintf(stderr, "GLFW ERROR!\nCode: %x\nDescription: %s\n", code, description);
}

void GLAPIENTRY debug_callback(GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar* message,
    const GLvoid* userParam)
{
    fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x\nmessage = %s\n\n", ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
};

int main(int argc, char** argv)
{
    std::string msg = "";
    int wWidth = 800;
    int wHeight = 600;
    bool drawTri = false;
    bool drawSqr = false;

    try 
    {
        TCLAP::CmdLine cmd("Prog2002 OpenGL Application Commands", ' ', "0.1");

        TCLAP::ValueArg<int> cmdWidth("", "width=", "Setting the width of the application window", false, 800, "int");
        TCLAP::ValueArg<int> cmdHeight("", "height=", "Setting the height of the application window", false, 600, "int");

        cmd.add(cmdWidth);
        cmd.add(cmdHeight);

        TCLAP::SwitchArg console("c", "console", "Launch the application in console mode", cmd, false);
        TCLAP::SwitchArg sqr("s", "sqr", "Enable the drawing of a square", cmd, false);
        TCLAP::SwitchArg tri("t", "tri", "Enable the drawing of a triangle", cmd, false);

        cmd.parse(argc, argv);
        bool runConsole = console.getValue();
        wWidth = cmdWidth.getValue();
        wHeight =  cmdHeight.getValue();
        drawTri = tri.getValue();
        drawSqr = sqr.getValue();

        if (runConsole)
        {
            msg = "Hello OpenGL: Console\n";
            std::cout << msg << "w: " << wWidth << " h: " << wHeight << std::endl;
        }
        else
        {
            msg = "Hello OpenGL\n";
            std::cout << msg << "w: " << wWidth << " h: " << wHeight <<  std::endl;
        }
    }
    catch(TCLAP::ArgException &e)
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
        return EXIT_FAILURE;
    }

    glfwSetErrorCallback(GLFWErrorCallback);

    if(!glfwInit())
    { 
        fprintf(stderr, "Failed to initialize glfw.\n");
        return EXIT_FAILURE;
    }

    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    GLFWwindow* window = glfwCreateWindow(wWidth, wHeight, "Hello OpenGL", nullptr, nullptr);
    if(!window)
    {
        fprintf(stderr, "Failed to create the window and the OpenGL context.\n");
        glfwTerminate();
        return EXIT_FAILURE;
    }

    glfwMakeContextCurrent(window);

    if(!gladLoadGL(glfwGetProcAddress))
    {
        fprintf(stderr, "Failed to initialize glad.\n");
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(debug_callback, 0);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    float vertices[9*6] = 
    {
        // square: Triangle 1
        0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f,    // Square colour
        -0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f,    // Square colour
        -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f,   // Square colour
        // Square: Triangle 2
        0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f,    // Square colour
        0.5f, 0.5f,  0.0f, 1.0f, 0.0f, 1.0f,    // Square colour
        -0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f,    // Square colour
                // Triangle
        -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f,   // Triangle colour
        0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f,    // Triangle colour
        0.0f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f,     // Triangle colour
    };

    // Vertex Array Object
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    // Vertex Attributes
    glVertexArrayAttribBinding(vao, 0, 0);
    glVertexArrayAttribBinding(vao, 1, 0);

    glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribFormat(vao, 1, 4, GL_FLOAT, GL_FALSE, sizeof(float)*2);
    glEnableVertexArrayAttrib(vao, 0);
    glEnableVertexArrayAttrib(vao, 1);
    // Vertex Buffer Object
    GLuint vboChessboard;
    glCreateBuffers(1, &vboChessboard);
    glBindBuffer(GL_ARRAY_BUFFER, vboChessboard);
    glNamedBufferStorage(vboChessboard, sizeof(vertices), vertices, GL_DYNAMIC_STORAGE_BIT);
    glVertexArrayVertexBuffer(vao, 0, vboChessboard, 0, sizeof(float)*6);
    
    // Vertex Shader 
    const GLchar* vs_source = R"(
        #version 430 core
    
        layout(location = 0) in vec2 pos;
        layout(location = 1) in vec4 col;
    
        out vec4 vs_col;
        void main()
        {
            vs_col = col;
            gl_Position = vec4(pos, 0.0, 1.0);
        }
    )";
    
    GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vShader, 1, &vs_source, 0);
    glCompileShader(vShader);
    GLint status = 0;
    glGetShaderiv(vShader, GL_COMPILE_STATUS, &status);
    if (!status)
    {
        GLchar info[1024];
        glGetShaderInfoLog(vShader, sizeof(info), 0, info);
        fprintf(stderr, "Error compiling the vertex shader.\nLOG: %s\n", info);
        return 0;
    }
    // Fragment Shader
    const GLchar* fs_source = R"(
        #version 430 core
    
        in vec4 vs_col;
        out vec4 color;
    
        void main()
        {
            color = vs_col;
        }
    )";
    
    GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fShader, 1, &fs_source, 0);
    glCompileShader(fShader);
    glGetShaderiv(fShader, GL_COMPILE_STATUS, &status);
    if (!status)
    {
        GLchar info[1024];
        glGetShaderInfoLog(fShader, sizeof(info), 0, info);
        fprintf(stderr, "Error compiling the fragment shader.\nLOG: %s\n", info);
        return 0;
    }
    // Rendering Program
    GLuint program = glCreateProgram();
    glAttachShader(program, vShader);
    glAttachShader(program, fShader);
    glLinkProgram(program);
    status = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (!status)
    {
        GLchar info[1024];
        glGetProgramInfoLog(program, sizeof(info), 0, info);
        fprintf(stderr, "Failed to link the rendering program.\nLOG: %s", info);
        return 0;
    }    
    glDeleteShader(vShader);
    glDeleteShader(fShader);
    glUseProgram(program);

    // Rendering loop
    float colour[] = {0.5f, 0.5f, 0.5f, 1.0f};
    float currentFrame = 0, deltaTime = 0, lastFrame = 0;

    float colourChange = 0.0f;

    do
    {
        glClearBufferfv(GL_COLOR, 0, colour);
        
        if(drawSqr)
                glDrawArrays(GL_TRIANGLES, 0, 6);
        if(drawTri)
        {
            currentFrame = (float)glfwGetTime();
            deltaTime = currentFrame - lastFrame;
            lastFrame = currentFrame;

            colourChange += 100.0f * deltaTime;

            float newTriCol[] = {
                cos((colourChange*(3.14f/180.f))), 
                0.0, 
                sin((colourChange*(3.14f/180.f))), 
                1.0f
            };

            for (int i = 6; i < 9; i++)
            {
                glNamedBufferSubData(vboChessboard, sizeof(float)*(2+(6*i)), sizeof(newTriCol), newTriCol);
            }
            
            glDrawArrays(GL_TRIANGLES, 6, 3);
        }

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while (!glfwWindowShouldClose(window));
    
    glfwDestroyWindow(window);
    glfwTerminate();

    return EXIT_SUCCESS; 
}